What is h2tools
=======
**H2-tools** is an open-source software designed to work with the **H2-** matrix format.

It contains:

1)  Adaptive H2-approximation using the adaptive nested cross approximation.  
2) Fast solvers for the system with an H2-matrix using preconditioned SE-form.

Main contributors
=================
Alexander Mikhalev  
Ivan Oseledets  
Daria Sushnikova  

Installation
============
##Download the code
This installation works with git submodules
To clone the repository, run following:
```
git clone https://muxas@bitbucket.org/muxas/h2tools.git
cd h2tools
git submodule init
git submodule update
```
##Install the package
The installation is handled through **setup.py** scripts
```
cd h2py
python setup.py install
```

or, if you want to have it compiled just in place, run

```
cd h2py
python setup.py build_ext --inplace
```

Examples
========
All examples are in **examples** folder.
Be sure to compile the package before running examples