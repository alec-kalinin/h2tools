# Hack to prevent bug of qr decomposition for 130x65 matrices (multithread mkl bug)
import os
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'

####
# h2tools_dir contains path to 'h2tools' package
####
h2tools_dir = '../../'

####
# pypropack_dir contains path to module 'pypropack'
####
pypropack_dir = h2tools_dir+'h2py/pypropack/'

####
# max_items is a maximum number of memory cells to use with naive_matvec
# max_items*8 is a number of bytes, used by naive_matvec function
# max_items = 500000000 is about to use 4GB of RAM
####
max_items = 500000000

####
# feps is a parameter, equal to (eps)/(1+eps), where 'eps' is inductive capacity
# special_const is a parameter used to compute solvation energy
####
feps = 78.5/79.5
special_const = 0.5*1.602176565e-19*1.602176565e-19*8.9875517873681764e+9*1.0e+10*6.02214129e+23/4184.0

####
# imports
####
import sys
sys.path.append(h2tools_dir)
sys.path.append(pypropack_dir)
import numpy as np
from scipy.sparse.linalg import gmres, LinearOperator
from time import time
from h2py.data.surface_particle import Data, electro, rhs, energy
from h2py.main import Problem
import os

####
# test_dir contains all the tests (currently, 1.1GB in archived form)
####
if len(sys.argv) < 3 or len(sys.argv) > 5:
    print 'Usage of this script is as follows:'
    print '\'python mcbhsolv.py accuracy_parameter surface_and_atoms_file [output_file] [charges_file]\''
    print 'examples:'
    print '\'python mcbhsolv.py 1e-4 1c5y_fl_surface_and_atoms_0.10.txt\''
    print '\'python mcbhsolv.py 1e-4 1c5y_fl_surface_and_atoms_0.10.txt 1c5y_fl_0.10.out\''
    print '\'python mcbhsolv.py 1e-4 1c5y_fl_surface_and_atoms_0.10.txt 1c5y_fl_0.10.out 1c5y_fl_charges_0.10.txt\''
    exit(0)
test_tau = float(sys.argv[1])
test_filename = sys.argv[2]
test_output = None
if len(sys.argv) > 3:
    test_output = sys.argv[3]
test_charges = None
if len(sys.argv) > 4:
    test_charges = sys.argv[4]

####
# function for solving matrix equations
####
def solve(surf, rhs, eps = 1e-6, maxiter = 999):
    def dot(x):
        return surf.dot(x)+surf.d*x
    def simple_print(r):
        print simple_print.__dict__['i'], r
        simple_print.__dict__['i'] += 1
        #print('res: %3.1e' % r)
    lo=LinearOperator((surf.row_data.count, surf.col_data.count), dot, dtype=rhs.dtype)
    simple_print.__dict__['i'] = 1
    #print 'in gmres'
    sol=gmres(lo, rhs, tol=eps, callback = simple_print, maxiter = maxiter)
    #print 'out gmres'
    w=sol[0]
    #print 'gmres info:', sol[1]
    return w, simple_print.__dict__['i']-1

sys.stdout.flush()
test_time = time()
problem = Data.from_files(test_filename, 78.5)
read_time = time()-test_time
print 'atoms:', problem.acount
print 'surface:', problem.count
print 'building tree'
sys.stdout.flush()
tree_time = time()
surf = Problem(electro, problem, block_size = 100)
sys.stdout.flush()
surf.gen_queue(symmetric = 0)
tree_time = time()-tree_time
print 'done'
print 'computing rhs'
sys.stdout.flush()
rhs_time = time()
linecount = (problem.count*problem.acount-1)/max_items+1
linesize = (problem.count-1)/linecount+1
l1 = np.arange(problem.acount, dtype = np.int32)
f = np.ndarray(problem.count)
for i in xrange(linecount):
    l = np.arange(linesize*i, min(linesize*(i+1), problem.count), dtype = np.int32)
    tmp = rhs(problem, l, problem, l1)
    f[l] = tmp.dot(problem.acharge)
    del tmp
rhs_time = time()-rhs_time
print 'done'
print 'computing Q^{T}D'
sys.stdout.flush()
qtd_time = time()
qtd = np.ndarray(problem.count)
for i in xrange(linecount):
    l = np.arange(linesize*i, min(linesize*(i+1), problem.count), dtype = np.int32)
    tmp = energy(problem, l, problem, l1)
    qtd[l] = tmp.dot(problem.acharge)
    del tmp
qtd_time = time()-qtd_time
print 'done'
print 'MCBH-SVD'
sys.stdout.flush()
mcbh_time = time()
surf.factorize('h2', tau = test_tau, iters = 1, onfly = 0)
mcbh_time = time()-mcbh_time
print 'done'
print surf.factor.nbytes()/1024./1024, 'MB'
print 'computing diagonal'
sys.stdout.flush()
diag_time = time()
surf.d = feps-surf.rdot(np.ones(problem.count))
diag_time = time()-diag_time
print 'done'
print 'solving'
sys.stdout.flush()
solve_time = time()
x = solve(surf, f, eps = test_tau)
solve_time = time()-solve_time
print 'done in', x[1], 'iterations'
test_time = time()-test_time
e = qtd.dot(x[0])*special_const
print 'energy', e
print 'read', read_time, 'secs'
print 'tree', tree_time, 'secs'
print 'rhs', rhs_time, 'secs'
print 'Q^{T}D', qtd_time, 'secs'
print 'MCBH', mcbh_time, 'secs'
print 'diag', diag_time, 'secs'
print 'solve', solve_time, 'secs'
print 'total', test_time, 'secs'
sys.stdout.flush()
new_elem = {'read_time':read_time, 'tree_time':tree_time,\
            'rhs_time':rhs_time, 'qtd_time':qtd_time, 'mcbh_time':mcbh_time,\
            'diag_time':diag_time, 'solve_time':solve_time, 'test_time':test_time,\
            'test_filename':test_filename, 'test_output':test_output, 'test_charges':test_charges,\
            'acount':problem.acount, 'count':problem.count, 'energy':e, 'test_accuracy':test_tau,\
            'iterations':x[1], 'far_mem':surf.factor.nbytes(),\
            'close_mem':surf.close_factor.nbytes()}
if not (test_output is None):
    f = open(test_output, 'w')
    f.write(str(new_elem)+'\n')
    f.close()
if not (test_charges is None):
    f = open(test_charges, 'w')
    f.write(str(problem.count)+'\n')
    for i in xrange(problem.count):
        f.write('{:.8e}\n'.format(x[0][i]))
    f.close()
    
