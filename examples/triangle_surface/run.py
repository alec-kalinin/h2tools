print 'This test script is running for about minutes on home computer'
print 'Please, be patient'
from time import time
start_time = time()

# Hack to prevent bug of qr decomposition for 130x65 matrices (multithread mkl bug)
import os
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'

# Main imports
import sys
import numpy as np
sys.path.append('../../')
from h2py.data.triangle_surface import Data, integral_inverse_r3
from h2py.main import Problem

# Main part of the test script
# reading triangle data from file
problem = Data.from_dat('Geodat.dat')
# setting interaction function
func0 = integral_inverse_r3

# building tree for sources and receivers (same for this test script)
print 'building tree'
surf = Problem(func0, problem, block_size=50)
# generating queue of computations for multicharge method
surf.gen_queue(symmetric=0)

# computing multicharge representation
print 'Computing MCBH'
surf.factorize('h2', tau=1e-4, iters=1, onfly=0, verbose=1)
print 'memory for uncompressed approximation: '+str(surf.factor.nbytes()/1024./1024)+' MB'

# computing relative spectral error of approximation to approximant
print 'computing error by pypropack, most timeconsuming operation'
print 'relative error of approximation:', surf.diffnorm2()

# copying multicharge factor
uncompressed = surf.factor.copy()

# compressing multicharge approximation
print 'compressing'
t0 = time()
surf.factor.svdcompress(1e-2, verbose=1)
print 'compress time:', time()-t0
print 'memory for SVD-compressed approximation: '+str(surf.factor.nbytes()/1024./1024)+' MB'

# computing relative spectral error of svd-compressed approximation to uncompressed approximation
print 'computing error by pypropack'
print 'relative error of SVD-compressed approximation to uncompressed approximation:', surf.diffnorm2(uncompressed)

# Test script finished
print 'Test script finished in {:.1f} seconds'.format(time()-start_time)
