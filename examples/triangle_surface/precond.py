import os
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
import sys

import numpy as np
from time import time
sys.path.append('../../')
from h2py.data.triangle_surface import Data, integral_inverse_r,integral_inverse_r3

problem = Data.from_dat('Geodat.dat')
func0 = integral_inverse_r3
#problem.count = 100
#problem.vertex = problem.vertex[:,:100]
from h2py.main import Problem
print 'building tree'
surf = Problem(func0, problem, block_size = 50)
print 'done'
print 'MCBH-SVD'
t0 = time()
surf.gen_queue(symmetric = False)
surf.factorize('h2', tau = 1e-1, iters = 1)
print 'memory consumption: ' + str(surf.factor.nbytes()/1024./1024) + ' MB'
print 'done in', time() - t0

from  h2py.core.ts import hyp
from  h2py.core.ts import sparse_gipermatrix_test
from  h2py.core.ts import show_hyper
from  h2py.core.ts import info_hyper
from  h2py.core.ts import solving
hyp(surf)
test_vec =np.ones(problem.count)#np.zeros(problem.count)# np.arange(problem.count)#
dot_test = surf.dot(test_vec, dasha_debug=1)
sparse_gipermatrix_test(surf,dot_test,test_vec)
'''
for i in xrange(1000):
	print "i = ",i 
	test_vec [i] = 1 
	#test_vec [i+1] = 1
	#test_vec [i+1] = 1 
	dot_test = surf.dot(test_vec)
	sparse_gipermatrix_test(surf,dot_test,test_vec)
#test_vec = np.zeros(problem.count)
#test_vec[100] = 1.0
###dot_test = surf.dot(test_vec)
###sparse_gipermatrix_test(surf,dot_test,test_vec)
#show_hyper(surf)
#solving(surf)
#info_hyper(surf)
import ipdb; ipdb.set_trace()'''

