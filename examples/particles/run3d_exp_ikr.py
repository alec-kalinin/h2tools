print 'This test script is running for about 10 minutes on my home computer'
print 'Please, be patient'
from time import time, sleep
start_time = time()
sleep(2)

# Hack to prevent bug of qr decomposition for 130x65 matrices (multithread mkl bug)
import os
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'

# Main imports
import sys
import numpy as np
sys.path.append('../../')
from h2py.data.particles import Data, exp_ikr
from h2py.main import Problem

# Main part of the test script
# reading triangle data from file
problem = Data.from_txt_file('data3d.txt')
from math import pi
problem.param_k = 25*pi
# setting interaction function
func0 = exp_ikr

# building tree for sources and receivers (same for this test script)
print 'building tree'
surf = Problem(func0, problem, block_size=20)
# generating queue of computations for multicharge method
surf.gen_queue(symmetric=0)

# computing multicharge representation
print 'Computing MCBH, relative error parameter tau set to 1e-4'
surf.factorize('h2', tau=1e-4, iters=1, onfly=0, verbose=1)
print 'memory for uncompressed approximation: '+str(surf.factor.nbytes()/1024./1024)+' MB'

"""
def randomcheck():
    x = np.random.rand(problem.count,2).view(np.complex128)[:,0]
    y0 = surf.naive_far_matvec(x)
    y1 = surf.factor.dot(x)
    print np.linalg.norm(y0-y1)/np.linalg.norm(y0)

for i in range(100):
    randomcheck()
"""

# computing relative spectral error of approximation to approximant
print 'computing error by pypropack, most timeconsuming operation'
print 'relative error of approximation:', surf.diffnorm2()

# copying multicharge factor
uncompressed = surf.factor.copy()

# compressing multicharge approximation
print 'compressing with relative error parameter set to 1e-2'
t0 = time()
surf.factor.svdcompress(1e-2, verbose=1)
print 'compress time:', time()-t0
print 'memory for SVD-compressed approximation: '+str(surf.factor.nbytes()/1024./1024)+' MB'

# computing relative spectral error of svd-compressed approximation to uncompressed approximation
print 'computing error by pypropack'
print 'relative error of SVD-compressed approximation to uncompressed approximation:', surf.diffnorm2(uncompressed)

# Test script finished
print 'Test script finished in {:.1f} seconds'.format(time()-start_time)

