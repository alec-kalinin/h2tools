import sys
import numpy as np
from time import time
sys.path.append('../../')
from h2py.data.particles import Data, inv_distance

problem = Data.from_txt_file('data3d.txt')
func0 = inv_distance
from h2py.main import Problem
print 'building tree'
surf = Problem(func0, problem, block_size = 20)
surf.gen_queue(symmetric = 0)
print 'done'
print 'MSKEL'
t0 = time()
surf.factorize('mskel', tau = 1e-4, onfly = 1)
print 'MSKEL factorization done in', time()-t0
print surf.factor.nbytes()/1024./1024, 'MB'
print 'computing error by pypropack'
print 'relative spectral error is', surf.diffnorm2()
