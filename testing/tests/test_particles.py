import os
print 'Changing directory to \'particles\''
os.chdir('particles')

def perform_check(fname, rseed, N, dim, block_size, tau, iters, onfly, symmetric, verbose, funcname):
    for i0 in rseed:
        for i1 in N:
            for i2 in dim:
                for i3 in block_size:
                    for i4 in tau:
                        for i5 in iters:
                            for i6 in onfly:
                                for i7 in symmetric:
                                    for i8 in verbose:
                                        for i9 in funcname:
                                            command = 'python {:s} {:d} {:d} {:d} {:d} {:e} {:d} {:b} {:b} {:b} {:s}'.format(fname, i0, i1, i2, i3, i4, i5, i6, i7, i8, i9)
                                            print command
                                            output = os.popen(command).readlines()
                                            print output[-1][:-1]

# Testing function 'factorize' of h2tools/h2py/core/h2.py for particles problem
# relative error is measured  with pypropack package (relative to naive multiplication)
rseed = [0,1]
N = [10000]
dim = [2]
block_size = [40,20,10]
tau = [1e-4, 1e-8]
iters = [0,1,2]
onfly = [0,1]
symmetric = [0,1]
verbose = [1]
funcname = ['log_distance']
perform_check('test_factorize.py', rseed, N, dim, block_size, tau, iters, onfly, symmetric, verbose, funcname)
