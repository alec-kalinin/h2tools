# Reading parameters, setting path for both module and pypropack
import sys
sys.path.append('../../..')
sys.path.append('../../../h2py/pypropack')
rseed = int(sys.argv[1])
N = int(sys.argv[2])
dim = int(sys.argv[3])
block_size= int(sys.argv[4])
tau = float(sys.argv[5])
iters = int(sys.argv[6])
onfly = bool(sys.argv[7])
symmetric = bool(sys.argv[8])
verbose = bool(sys.argv[9])
funcname = sys.argv[10]

print 'This test script is running for about 4 minutes on my home computer'
print '     Please, be patient'
print 'Parameters:'
print 'randseed={:d}, N={:d}, dim={:d}, block_size={:d}, tau={:e}, iters={:d}, onfly={:b}, symmetric={:b}, verbose={:b}'.format(rseed, N, dim, block_size, tau, iters, onfly, symmetric, verbose)
from time import time, sleep
start_time = time()
sleep(3)

# Hack to prevent bug of qr decomposition for 130x65 matrices (multithread mkl bug)
import os
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'

# Main imports
import numpy as np
exec('from h2py.data.particles import Data, {:s}'.format(funcname))
from h2py.main import Problem

# Main part of the test script
# generating particles data
np.random.seed(rseed)
particles = np.random.rand(dim,N)
problem = Data(particles)
# setting interaction function
exec('func0 = {:s}'.format(funcname))

# building tree for sources and receivers (same for this test script)
print 'building tree'
surf = Problem(func0, problem, block_size=block_size)
# generating queue of computations for multicharge method
surf.gen_queue(symmetric=symmetric)

# computing multicharge representation
print 'Computing MCBH, relative error parameter tau set to {:e}'.format(tau)
surf.factorize('h2', tau=tau, iters=iters, onfly=onfly, verbose=1)
print 'memory for uncompressed approximation: '+str(surf.factor.nbytes()/1024./1024)+' MB'

# computing relative spectral error of approximation to approximant
print 'computing error by pypropack (relative spectral error), most timeconsuming operation'
res = surf.diffnorm2()
print 'relative error of approximation:', res

# Test script finished
print 'Test script finished in {:.1f} seconds'.format(time()-start_time)

if res < 10*tau:
    print 'Success'
else:
    print 'Failure'
