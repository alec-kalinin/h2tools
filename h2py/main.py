import numpy as np
from time import time
from scipy.sparse.linalg import gmres, LinearOperator
from scipy.sparse import csr_matrix
from tree import Tree
import core.close as close
import core.h2 as h2
import core.mskel as mskel
import sys
import scipy.sparse.linalg as la

try:
    from pypropack import svdp
except:
    pass

class Problem(object):
    def __init__(self, func, row_data, col_data = None, **kwargs):
        self.__func = func
        self.row_data = row_data
        self.col_data = col_data
        self.row_tree = Tree(row_data, row_data.bounding_box(np.arange(row_data.count)), **kwargs)
        if col_data is None:
            self.col_tree = self.row_tree
            self.col_data = self.row_data
        else:
            self.col_tree = Tree(col_data, col_data.bounding_box(np.arange(col_data.count)), **kwargs)
        l = np.arange(1, dtype=np.int32)
        dtype = self.func(l, l).dtype.type
        self.dtype = dtype
        self.row_tree.build(self.col_tree)
        
    def func(self, row, col):
        """Shortcut to function, matrix-like proxy to function"""
        return self.__func(self.row_data, row, self.col_data, col)

    def __del__(self):
        pass

    def factorize(self, factor_name, tau = 1e-4, **kwargs):
        close_keys = {'onfly', 'close_onfly'}
        h2_keys = {'onfly', 'iters', 'verbose', 'rect_maxvol_tol'}
        mskel_keys = {'verbose'}
        keys = {'close': close_keys, 'h2': h2_keys, 'mskel': mskel_keys}
        kwarg_keys = set(kwargs.keys())
        far_keys = keys[factor_name].intersection(kwarg_keys)
        close_keys = close_keys.intersection(kwarg_keys)
        all_keys = far_keys.union(close_keys)
        wrong_keys = kwarg_keys.difference(all_keys)
        if len(wrong_keys) > 0:
            raise KeyError("wrong keys for factorize function: {}".format(list(wrong_keys)))
        far_kwargs = {}
        for i in far_keys:
            far_kwargs[i] = kwargs[i]
        close_kwargs = {}
        for i in close_keys:
            close_kwargs[i] = kwargs[i]
        self.close_factor = close.Factor(self.dtype, self.func, self.row_data, self.row_tree, self.col_data, self.col_tree, **close_kwargs)
        if factor_name == 'h2':
            self.factor = h2.Factor(self.dtype, self.func, self.row_data, self.row_tree, self.col_data, self.col_tree, self.queue, tau, symmetric=self.symmetric, **far_kwargs)
        elif factor_name == 'mskel':
            self.factor = mskel.Factor(self.dtype, self.func, self.row_data, self.row_tree, self.col_data, self.col_tree, self.queue, tau, **far_kwargs)
        
    def save(self):
        pass
    def load(self):
        pass
        
    def diffnorm2(self, factor2 = None):
        try:
            svdp
        except NameError:
            raise Warning("No pypropack installed, cannot measure error")
        if factor2 is None:
            linop_diff = la.LinearOperator((self.row_data.count, self.col_data.count), matvec=lambda x:self.naive_far_matvec(x)-self.factor.dot(x), rmatvec=lambda x:self.naive_far_matvec(x,T=1)-self.factor.rdot(x), dtype = self.dtype)
        else:
            linop_diff = la.LinearOperator((self.row_data.count, self.col_data.count), matvec=lambda x:factor2.dot(x)-self.factor.dot(x), rmatvec=lambda x:factor2.rdot(x)-self.factor.rdot(x), dtype = self.dtype)
        s_diff = svdp(linop_diff, 1, compute_u=0, compute_v=0, kmax = 100)
        linop_factor = la.LinearOperator((self.row_data.count, self.col_data.count), matvec=lambda x:self.factor.dot(x), rmatvec=lambda x:self.factor.rdot(x), dtype = self.dtype)
        s_factor = svdp(linop_factor, 1, compute_u=0, compute_v=0, kmax = 100)
        return s_diff[0][0]/s_factor[0][0]
        
    def naive_matvec(self, x, max_items = 125000000, T=False):
        count = (self.row_data.count*self.col_data.count-1)/max_items+1
        if T:
            col_step = (self.col_data.count-1)/count+1
            if len(x.shape) == 1:
                ans = np.ndarray(self.col_data.count, dtype = x.dtype)
            else:
                ans = np.ndarray((self.col_data.count, x.shape[1]), dtype = x.dtype)
            full_row = np.arange(self.row_data.count, dtype = np.int32)
            for i in xrange(count):
                l = np.arange(col_step*i, min(col_step*(i+1), self.col_data.count), dtype = np.int32)
                ans[l] = self.func(full_row, l).T.dot(x)
        else:
            row_step = (self.row_data.count-1)/count+1
            if len(x.shape) == 1:
                ans = np.ndarray(self.row_data.count, dtype = x.dtype)
            else:
                ans = np.ndarray((self.row_data.count, x.shape[1]), dtype = x.dtype)
            full_col = np.arange(self.col_data.count, dtype = np.int32)
            for i in xrange(count):
                l = np.arange(row_step*i, min(row_step*(i+1), self.row_data.count), dtype = np.int32)
                ans[l] = self.func(l, full_col).dot(x)
        return ans

    def naive_far_matvec(self, x, T = False):
        row = self.row_tree
        col = self.col_tree
        row_data = self.row_data
        col_data = self.col_data
        row_size = row.level[-1]
        col_size = col.level[-1]
        func = self.func
        dtype = self.dtype
        if x.ndim is 1:
            x = x.reshape(-1, 1)
        nrhs = x.shape[1]
        if T:
            answer = np.zeros((col.index[0].shape[0], nrhs), dtype = dtype)
            for i in xrange(col_size):
                for j in col.far[i]:
                    answer[col.index[i]] += func(row.index[j], col.index[i]).T.dot(x[row.index[j]])
        else:
            answer = np.zeros((row.index[0].shape[0], nrhs), dtype = dtype)
            for i in xrange(row_size):
                for j in row.far[i]:
                    answer[row.index[i]] += func(row.index[i], col.index[j]).dot(x[col.index[j]])
        if answer.shape[1] is 1:
            answer = answer.reshape(-1)
        return answer
        
    def dot(self, x, dasha_debug=False):
        return self.factor.dot(x, dasha_debug)+self.close_factor.dot(x)
        
    def rdot(self, x, dasha_debug=False):
        return self.factor.rdot(x, dasha_debug)+self.close_factor.rdot(x)

    def gen_queue(self, symmetric=False):
        level_count = len(self.row_tree.level)-2
        self.symmetric = symmetric
        if symmetric:
            self.queue = [[] for i in xrange(level_count)]
            for i in xrange(level_count):
                for j in xrange(self.row_tree.level[level_count-i-1], self.row_tree.level[level_count-i]):
                    if not self.row_tree.notransition[j]:
                        self.queue[i].append(('row', j))
        else:
            self.queue = [[] for i in xrange(2*level_count)]
            for i in xrange(level_count):
                for j in xrange(self.row_tree.level[level_count-i-1], self.row_tree.level[level_count-i]):
                    if not self.row_tree.notransition[j]:
                        self.queue[2*i].append(('row', j))
                for j in xrange(self.col_tree.level[level_count-i-1], self.col_tree.level[level_count-i]):
                    if not self.col_tree.notransition[j]:
                        self.queue[2*i+1].append(('col', j))
        i = len(self.queue)-1
        while i >= 0:
            if self.queue[i] == []:
                self.queue.pop(i)
            i -= 1
    """
    def csr_diagprecond(self, rhs):
        if (self.col_data is None):
            col_size = None
        else:
            col_size = self.col_data.count
        row_size = self.row_data.count
        list0 = np.ndarray(1, dtype = np.int32)
        d = csr_matrix((row_size, row_size), dtype = np.float64)
        row = self.row_data
        size = row_size
        if self.col_tree is None:
            col = row
        else:
            col = self.col_data
            size = min(size, col_size)
        for i in xrange(size):
            list0[0] = i
            elem = core.fill_matrix(self.func, row, list0, col, list0)[0,0]
            if abs(elem) < 1e-15:
                print i 
            d[i,i] = 1.0/elem
        newrhs = d.dot(rhs)
        return d, newrhs

    def solve_csrprecond(self, rhs, prec, eps = 1e-3, start = None, maxiter = 1000, M = None):
        if (self.col_data is None):
            col_size = None
        else:
            col_size = self.col_data.count
        row_size = self.row_data.count
        def dot(x):
            return prec.dot(self.dot(x))
        if col_size is None:
            lo=LinearOperator( (row_size, row_size),dot,dtype=rhs.dtype)
        else:
            lo=LinearOperator( (row_size, col_size),dot,dtype=rhs.dtype)
        print 'in gmres'
        def simple_print(r):
            print r
            #print('res: %3.1e' % r)
        sol=gmres(lo,rhs,x0=start,tol=eps, callback = simple_print, maxiter = maxiter, M = M)
        print 'out gmres'
        w=sol[0]
        print 'gmres info:', sol[1]
        return w

    def solve(self, rhs, eps = 1e-3, start = None, maxiter = 1000):
        if (self.col_data is None):
            col_size = None
        else:
            col_size = self.col_data.count
        row_size = self.row_data.count
        if col_size is None:
            lo=LinearOperator( (row_size, row_size),self.dot,dtype=rhs.dtype)
        else:
            lo=LinearOperator( (row_size, col_size),self.dot,dtype=rhs.dtype)
        print 'in gmres'
        def simple_print(r):
            print r
            #print('res: %3.1e' % r)
        sol=gmres(lo,rhs,x0=start,tol=eps, callback = simple_print, maxiter = maxiter)
        print 'out gmres'
        w=sol[0]
        print 'gmres info:', sol[1]
        return w
    """
