cimport numpy as np
import numpy as np
from time import time
from math import sqrt
from libc.stdlib cimport malloc, free
from libc.math cimport sqrt, fabs
from h2py.data.integ import integr
#import ipdb; ipdb.set_trace()
#from integ import INTEGR 

def integral_inverse_r3(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    integral_inverse_r3_3d(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans

def integral_inverse_r(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    integral_inverse_r_3d(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans

cdef int integral_inverse_r3_3d_side(double *x, double *x1, double *x2, double *out):
    cdef double rb = 0
    cdef double d01[3], d02[3], d21[3], r01, r02, a[3], ra, b1, b2, b
    cdef double rb2,rr,r21
    rb2 = rb*rb
    d01[0] = x[0]-x1[0]
    d01[1] = x[1]-x1[1]
    d01[2] = x[2]-x1[2]
    d02[0] = x[0]-x2[0]
    d02[1] = x[1]-x2[1]
    d02[2] = x[2]-x2[2]
    d21[0] = x2[0]-x1[0]
    d21[1] = x2[1]-x1[1]
    d21[2] = x2[2]-x1[2]
    r01 = d01[0]*d01[0]+d01[1]*d01[1]+d01[2]*d01[2]
    r02 = d02[0]*d02[0]+d02[1]*d02[1]+d02[2]*d02[2]
    r21 = d21[0]*d21[0]+d21[1]*d21[1]+d21[2]*d21[2]
    a[0] = d01[1]*d21[2]-d01[2]*d21[1]
    a[1] = d01[2]*d21[0]-d01[0]*d21[2]
    a[2] = d01[0]*d21[1]-d01[1]*d21[0]
    ra = a[0]*a[0]+a[1]*a[1]+a[2]*a[2]
    b1 = d01[0]*d21[0]+d01[1]*d21[1]+d01[2]*d21[2]
    b2 = d02[0]*d21[0]+d02[1]*d21[1]+d02[2]*d21[2]
    if ra < 1e-32:
        #print 'that\'s why'
        return 0
    rr = ra/r21
    b = (b1/sqrt(r01)-b2/sqrt(r02))/ra
    if rr < rb2:
        rr=rr/rb2
        rr=rr*(2-rr)
    else:
        rr=1
    out[0] -= rr*b*a[0]
    out[1] -= rr*b*a[1]
    out[2] -= rr*b*a[2]
    return 0

cdef double integral_inverse_r3_3d_elem(double *x, double *nx, int size, double *x0):
    cdef int i
    cdef double *dl = [0, 0, 0], ans = 0
    for i in range(size-1):
        integral_inverse_r3_3d_side(x, x0+3*i, x0+3*i+3, dl)
    integral_inverse_r3_3d_side(x, x0+3*size-3, x0, dl)
    ans = nx[0]*dl[0]+nx[1]*dl[1]+nx[2]*dl[2]
    return ans
    
cdef double integral_inverse_r_3d_elem(double *x, double *nx, int size, double *x0):
    cdef int i
    cdef double *dl = [0, 0, 0], ans = 0
    
    for i in range(size-1):
        integral_inverse_r3_3d_side(x, x0+3*i, x0+3*i+3, dl)
    integral_inverse_r3_3d_side(x, x0+3*size-3, x0, dl)
    ans = nx[0]*dl[0]+nx[1]*dl[1]+nx[2]*dl[2]
    return ans
    


cdef int integral_inverse_r3_3d(void *obj1, int row_size, int *row, void *obj2, int col_size, int *col, double *out):
    cdef object pyobj1 = <object>obj1, pyobj2 = <object>obj2
    cdef int src_vertex_count = <int>pyobj2.vertex.shape[1]
    cdef double *src_vertex_x = <double *><long>(pyobj2.vertex).ctypes.data
    cdef double *src_vertex_y = src_vertex_x+src_vertex_count
    cdef double *src_vertex_z = src_vertex_y+src_vertex_count
    cdef int *src_p = <int *><long>(pyobj2.polygon).ctypes.data
    cdef int *src_p_start = <int *><long>(pyobj2.polygon_start).ctypes.data
    cdef int *src_p_size = <int *><long>(pyobj2.polygon_size).ctypes.data
    cdef int dst_count = <int>pyobj1.polygon_count
    cdef double *dst_collocation_x = <double *><long>(pyobj1.polygon_collocation).ctypes.data
    cdef double *dst_collocation_y = dst_collocation_x+dst_count
    cdef double *dst_collocation_z = dst_collocation_y+dst_count
    cdef double *dst_area = <double *><long>(pyobj1.polygon_area.ctypes.data)
    cdef double *dst_normal_x = <double *><long>(pyobj1.polygon_normal).ctypes.data
    cdef double *dst_normal_y = dst_normal_x+dst_count
    cdef double *dst_normal_z = dst_normal_y+dst_count
    cdef int i, j, vertex_index, k, tmpk, p_start, p_size, max_p_size = 0
    cdef int *p
    cdef double x[3], nx[3], *x0
    cdef double tmp
    for i in range(col_size):
        p_size = src_p_size[col[i]]
        if p_size > max_p_size:
            max_p_size = p_size
    x0 = <double *>malloc(3*max_p_size*sizeof(double))
    for i in range(col_size):
        k = col[i]
        p_start = src_p_start[k]
        p_size = src_p_size[k]
        p = src_p+p_start
        x0[0] = 0
        x0[1] = 0
        x0[2] = 0
        for j in range(p_size):
            k = p[j]
            #x0[0] += src_vertex_x[k]/p_size
            #x0[1] += src_vertex_y[k]/p_size
            #x0[2] += src_vertex_z[k]/p_size
            x0[3*j] = src_vertex_x[k]
            x0[3*j+1] = src_vertex_y[k]
            x0[3*j+2] = src_vertex_z[k]
        for j in range(row_size):
            k = row[j]
            x[0] = dst_collocation_x[k]
            x[1] = dst_collocation_y[k]
            x[2] = dst_collocation_z[k]
            nx[0] = dst_normal_x[k]
            nx[1] = dst_normal_y[k]
            nx[2] = dst_normal_z[k]
            #out[j*col_size+i] = integral_inverse_r3_3d_elem(x, nx, p_size, x0)
            out[j*col_size+i] = dst_area[k]*integral_inverse_r3_3d_elem(x, nx, p_size, x0)
            #tmp = sqrt((x[0]-x0[0])*(x[0]-x0[0])+(x[1]-x0[1])*(x[1]-x0[1])+(x[2]-x0[2])*(x[2]-x0[2]))
            #if tmp < 1e-32:
            #    out[j*col_size+i] = 0
            #else:
            #    out[j*col_size+i] = 1.0/(tmp)
            #out[j*col_size+i] = fabs(integral_inverse_r3_3d_elem(x, nx, p_size, x0))
            #if col[i] != row[j]:
            #    out[j*col_size+i] = -out[j*col_size+i]
    free(x0)
    return 0


cdef int integral_inverse_r_3d(void *obj1, int row_size, int *row, void *obj2, int col_size, int *col, double *out):
    cdef object pyobj1 = <object>obj1, pyobj2 = <object>obj2
    cdef int src_vertex_count = <int>pyobj2.vertex.shape[1]
    cdef double *src_vertex_x = <double *><long>(pyobj2.vertex).ctypes.data
    cdef double *src_vertex_y = src_vertex_x+src_vertex_count
    cdef double *src_vertex_z = src_vertex_y+src_vertex_count
    cdef int *src_p = <int *><long>(pyobj2.polygon).ctypes.data
    cdef int *src_p_start = <int *><long>(pyobj2.polygon_start).ctypes.data
    cdef int *src_p_size = <int *><long>(pyobj2.polygon_size).ctypes.data
    cdef int dst_count = <int>pyobj1.polygon_count
    cdef double *dst_collocation_x = <double *><long>(pyobj1.polygon_collocation).ctypes.data
    cdef double *dst_collocation_y = dst_collocation_x+dst_count
    cdef double *dst_collocation_z = dst_collocation_y+dst_count
    cdef double *dst_area = <double *><long>(pyobj1.polygon_area.ctypes.data)
    cdef double *dst_normal_x = <double *><long>(pyobj1.polygon_normal).ctypes.data
    cdef double *dst_normal_y = dst_normal_x+dst_count
    cdef double *dst_normal_z = dst_normal_y+dst_count
    cdef int i, j, vertex_index, k, tmpk, p_start, p_size, max_p_size = 0
    cdef int *p
    cdef double x[3], nx[3], *x0
    cdef double tmp
    for i in range(col_size):
        p_size = src_p_size[col[i]]
        if p_size > max_p_size:
            max_p_size = p_size
    x0 = <double *>malloc(3*max_p_size*sizeof(double))
    for i in range(col_size):
        k = col[i]
        p_start = src_p_start[k]
        p_size = src_p_size[k]
        p = src_p+p_start
        x0[0] = 0
        x0[1] = 0
        x0[2] = 0
        for j in range(p_size):
            k = p[j]
            #x0[0] += src_vertex_x[k]/p_size
            #x0[1] += src_vertex_y[k]/p_size
            #x0[2] += src_vertex_z[k]/p_size
            x0[3*j] = src_vertex_x[k]
            x0[3*j+1] = src_vertex_y[k]
            x0[3*j+2] = src_vertex_z[k]
        for j in range(row_size):
            k = row[j]
            x[0] = dst_collocation_x[k]
            x[1] = dst_collocation_y[k]
            x[2] = dst_collocation_z[k]
            nx[0] = dst_normal_x[k]
            nx[1] = dst_normal_y[k]
            nx[2] = dst_normal_z[k]
            
            
            
            
            #out[j*col_size+i] = dst_area[k]*integral_inverse_r_3d_elem(x, nx, p_size, x0)
            #import ipdb; ipdb.set_trace()
            temp = integr([x0[0],x0[1],x0[2]],[x0[3],x0[4],x0[5]],[x0[6],x0[7],x0[8]],3,x[0],x[1],x[2],2)
            out[j*col_size+i] = dst_area[k]*temp[1]*(1+1e-10*np.random.rand())
            #print INTEGR([0,0,0],[0,0,1],[0,1,0],3,1,1,1)
            #import ipdb; ipdb.set_trace()
    free(x0)
    return 0


class Data(object):
    def __init__(self, vertex, polygon):
        self.vertex = vertex.copy()
        self.polygon = polygon.copy()
        self.triangle_count = count = polygon.size/4
        self.polygon_count = count
        self.count = count
        v_count = vertex.shape[1]
        self.vertex_count = v_count
        self.dim = vertex.shape[0]
        self.triangle = self.polygon.reshape(-1,4)[:,1:4].flatten('C')
        self.polygon_size = 3*np.ones(count, dtype = np.int32)
        self.polygon_start = np.arange(1, 4*count, 4, dtype = np.int32)
        tmp_edge = []
        for i in xrange(count):
            p0, p1, p2 = self.triangle[3*i:3*i+3]
            tmp_edge.append((min(p0, p1), max(p0, p1)))
            tmp_edge.append((min(p2, p1), max(p2, p1)))
            tmp_edge.append((min(p0, p2), max(p0, p2)))
        self.hash = h = np.ndarray(3*count, dtype = np.int32)
        for i in xrange(count):
            h0 = tmp_edge[3*i][0]*v_count+tmp_edge[3*i][1]
            h1 = tmp_edge[3*i+1][0]*v_count+tmp_edge[3*i+1][1]
            h2 = tmp_edge[3*i+2][0]*v_count+tmp_edge[3*i+2][1]
            h[3*i:3*i+3] = h0, h1, h2
        ind = h.argsort()
        last = -1
        e_count = -1
        edge = []
        #print tmp_edge[0:6]
        self.polygon_edge = pe = np.ndarray(3*count, dtype = np.int32)
        for i in ind:
            if h[i] == last:
                pe[i] = e_count
                #print i, e_count, last
            else:
                e_count += 1
                last = h[i]
                edge.append(tmp_edge[i])
                pe[i] = e_count
                #e_count += 1
                #print 'new', i, e_count, last
        e_count += 1
        self.edge = np.array(edge)
        self.edge_count = e_count
        self.polygon_collocation = pc = np.ndarray((self.dim, count))
        ans = self.polygon_area = np.zeros(count)
        ans2 = self.polygon_normal = np.ndarray((self.dim, count))
        for i in xrange(count):
            v = self.vertex[:,self.triangle[3*i:3*i+3]]
            pc[:,i] = v.mean(axis = 1)
            x = v[:,1:3]-v[:,0].reshape(-1,1)
            tmp = np.array([x[1,1]*x[2,0]-x[2,1]*x[1,0], x[2,1]*x[0,0]-x[0,1]*x[2,0], x[0,1]*x[1,0]-x[1,1]*x[0,0]])
            #tmp = np.array([x2[1]*x1[2]-x2[2]*x1[1],x2[2]*x1[0]-x2[0]*x1[2],x2[0]*x1[1]-x2[1]*x1[0]])
            ans[i] = np.linalg.norm(tmp)
            ans2[:,i] = tmp/ans[i]
        ans *= 0.5

    @staticmethod
    def from_dat(f):
        if type(f) is str:
            f = open(f, 'r')
        p_count = 0
        v_count = 0
        polygon = []
        vertex = []
        #read number of objects
        n_objects = int(f.readline())
        print 'n_objects', n_objects
        for i_obj in xrange(n_objects):
            #read number of modules for each object
            n_modules = int(f.readline())
            print 'n_modules', n_modules
            for i_mod in xrange(n_modules):
                #read unboundness, n, n1, n2 (n = n1 * n2)
                f.readline()
                n_quad = int(f.readline())
                #print 'n_quad', n_quad
                f.readline()
                for i_quad in xrange(n_quad):
                    tmp = f.readline().split(' ')
                    #print tmp
                    tmp.pop()
                    for i in xrange(len(tmp)):
                        tmp[i] = float(tmp[i])
                    stmp = np.array(tmp).reshape(4,3)
                    b01 = int((stmp[0] == stmp[1]).all())
                    b02 = int((stmp[0] == stmp[2]).all())
                    b03 = int((stmp[0] == stmp[3]).all())
                    b12 = int((stmp[1] == stmp[2]).all())
                    b13 = int((stmp[1] == stmp[3]).all())
                    b23 = int((stmp[2] == stmp[3]).all())
                    if (b01+b02+b03+b12+b13+b23)/int(True) > 1:
                        print 'nothing should be added'
                        continue
                    if b01 == 1 or b02 == 1:
                        tmp[:3] = tmp[-3:]
                    if b12 == 1:
                        tmp[3:6] = tmp[-3:]
                    if (b01+b02+b03+b12+b13+b23)/int(True) == 1:
                        vertex.extend(tmp[:-3])
                        polygon.extend([3, v_count, v_count+1, v_count+2])
                        v_count += 3
                        p_count += 1
                        continue
                    vertex.extend(tmp)
                    polygon.extend([3, v_count, v_count+1, v_count+2, 3, v_count+2, v_count+3, v_count])
                    v_count += 4
                    p_count += 2
                #f.readline()
        p = np.array(polygon, dtype = np.int32)
        v = np.array(vertex).reshape(len(vertex)/3, 3).T.copy()
        f.close()
        self = Data(v, p)
        self.dim = 3
        return self
    
    def find_hyperplane(self, index):
        if index is None:
            collocation = self.polygon_collocation
            area = self.polygon_area
            index = np.arange(self.count)
        else:
            collocation = self.polygon_collocation[:,index]
            area = self.polygon_area[index]
        center = collocation.dot(area).reshape(self.dim, 1)/area.sum()
        #print 'center', center
        tri = self.triangle
        tmp = np.ndarray((self.dim, 3*index.size), dtype = np.float)
        tmp_a = np.array([[sqrt(2), 0, 0], [0.5*sqrt(2), sqrt(3.0/2), 0],[0.5*sqrt(2), sqrt(1.0/6), 2*sqrt(1.0/3)]])
        #index0 = p[(np.array([1,2,3]).reshape(3,1)+4*index).flatten()]
        t0 = time()
        for i in xrange(index.size):
            j = index[i]
            v = self.vertex[:,tri[3*j:3*j+3]]-center
            #v = self.vertex[:,index0[3*i:3*i+3]]-center
            #print 'vertex', v
            tmp[:,3*i:3*i+3] = sqrt(area[i])*(v.dot(tmp_a))
        u = np.linalg.svd(tmp, full_matrices = False)[0][:,0]
        t1 = time()
        #print 'find 0', t1-t0
        return center.reshape(self.dim), u
    
    def divide_hyperplane(self, index, c, u, tol = 1):
        index_start = index
        old_v_count = self.vertex.shape[1]
        old_e_count = self.edge_count
        old_t_count = self.triangle_count
        if index_start is None:
            edge_index = self.polygon_edge
            index = np.arange(self.count)
        else:
            edge_index = self.polygon_edge[(np.arange(3).reshape(3,1)+3*index).flatten('F')]
        ind = edge_index.argsort()
        last_edge = -1
        new_vertex_count = 0
        new_vertex = []
        new_constant_p = []
        edge_new_vertex_index = np.ndarray(edge_index.size, dtype = np.int32)
        last_vertex_index = -1
        failed_tol = 0
        t00 = 0
        t01 = 0
        t0 = time()
        c1 = c.reshape(-1,1)
        new_edge = []
        new_edge_count = 0
        #tmp_v = self.vertex[:,self.edge[edge_index]].reshape(3,-1)
        cu = c.dot(u)
        #tmp_scal = u.dot(tmp_v).reshape(-1,2)-cu
        for j in xrange(ind.size):
            i = ind[j]
            if edge_index[i] == last_edge:
                edge_new_vertex_index[i] = last_vertex_index
                #print 'again divide edge', i, edge_index[i], last_vertex_index
                continue
            last_edge = edge_index[i]
            edge_vertex = self.edge[last_edge]
            tt0 = time()
            #p0, p1 = tmp_scal[i]
            #v = tmp_v[:,2*i:2*i+2]
            #v0 = v[:,0]
            #v1 = v[:,1]
            v0 = self.vertex[:,edge_vertex[0]]
            v1 = self.vertex[:,edge_vertex[1]]
            #v = self.vertex[:,edge_vertex]
            #p0 = v[:,0].dot(u)
            #p1 = v[:,1].dot(u)
            #p0, p1 = tmp_scal[edge_vertex]-cu
            p0 = u.dot(v0)-cu
            p1 = u.dot(v1)-cu
            t01 += time()-tt0
            if p0*p1 > -1e-15:
                last_vertex_index = -1
                edge_new_vertex_index[i] = -1
                #print 'here', i
                #print 'one side', i, edge_index[i]
                continue
            p = p0/(p0-p1)
            if p < tol:
                last_vertex_index = -2-edge_vertex[0]
                edge_new_vertex_index[i] = last_vertex_index
                continue
            if 1-p < tol:
                last_vertex_index = -2-edge_vertex[1]
                edge_new_vertex_index[i] = last_vertex_index
                failed_tol += 1
                #print 'bad tol', i, edge_index[i]
                continue
            tt0 = time()
            edge_new_vertex_index[i] = new_vertex_count
            last_vertex_index = new_vertex_count
            new_vertex.append((v0+p*(v1-v0)).reshape(3,1))
            new_constant_p.append(p)
            new_edge.append((edge_vertex[0], old_v_count+new_vertex_count))
            new_edge.append((edge_vertex[1], old_v_count+new_vertex_count))
            new_edge_count += 2
            new_vertex_count += 1
            t00 += time()-tt0
            #print 'divide edge', i, edge_index[i], last_vertex_index
            #print v, v[:,0]+p*(v[:,1]-v[:,0]), p
        #print 'divide 0', t1-t0
        #print 'divide 00', t00, t01
        new_triangle_count = 0
        new_vertex_count2 = 0
        for i in xrange(index.size):
            tmp = edge_new_vertex_index[3*i:3*i+3]
            test = (tmp >= 0).sum()
            if test == 3:
                raise NameError("NEVER SEEN THIS BEFORE")
            new_triangle_count += test
            if test == 0:
                tmp2 = tmp.argsort()
                if tmp[tmp2[0]] != tmp[tmp2[1]]:
                    new_triangle_count += 1
                    new_vertex_count2 += 1
        if new_triangle_count == 0:
            #print 'no division at all'
            if index_start is None:
                s = u.dot(self.polygon_collocation)
            else:
                s = u.dot(self.polygon_collocation[:,index])
            permute = s.argsort()
            k = s[permute].searchsorted(u.dot(c))
            if index_start is None:
                return permute[:k], permute[k:]
            else:
                return index[permute[:k]], index[permute[k:]]
        t0 = time()
        #print new_vertex
        vertex = np.ndarray((self.dim, old_v_count+new_vertex_count+new_vertex_count2), dtype = np.float)
        vertex[:,:old_v_count] = self.vertex
        if len(new_vertex) > 0:
            vertex[:,old_v_count:old_v_count+new_vertex_count] = np.hstack(new_vertex)
        self.vertex = vertex
        #print 'edges not divided cause of \'tol\':', failed_tol
        #print 'old_t_count:', old_t_count
        #print 'new triangles count:', new_triangle_count
        new_triangle = np.ndarray(3*(old_t_count+new_triangle_count), dtype = np.int32)
        new_triangle[:3*old_t_count] = self.triangle
        self.triangle = new_triangle
        new_p_e = np.ndarray(3*(old_t_count+new_triangle_count), dtype = np.int32)
        new_p_e[:3*old_t_count] = self.polygon_edge
        self.polygon_edge = new_p_e
        next_triangle_index = old_t_count
        self.triangle_count += new_triangle_count
        new_normal = np.ndarray((self.dim, self.triangle_count), dtype = np.float)
        new_normal[:,:old_t_count] = self.polygon_normal
        self.polygon_normal = new_normal
        new_area = np.ndarray(self.triangle_count, dtype = np.float)
        new_area[:old_t_count] = self.polygon_area
        self.polygon_area = new_area
        new_collocation = np.ndarray((self.dim, self.triangle_count), dtype = np.float)
        new_collocation[:,:old_t_count] = self.polygon_collocation
        self.polygon_collocation = new_collocation
        t1 = time()
        #print 'divide 1', t1-t0
        t0 = time()
        tmp_v_count = old_v_count+new_vertex_count
        new_vertex = []
        new_vertex_count = 0
        for i in xrange(index.size):
            tv = edge_new_vertex_index[3*i:3*i+3]+old_v_count
            test = (tv < old_v_count).sum()
            te = edge_index[3*i:3*i+3].copy()
            tt = self.triangle[3*index[i]:3*index[i]+3].copy()
            if test == 0:
                print 'Something is buggy in my program'
                raise NameError("BUG")
            if test == 3:
                if (tv < old_v_count-1).sum() == 3:
                    raise NameError("ANOTHER BUG")
                while tv[2] != old_v_count-1:
                    tv = np.array([tv[1], tv[2], tv[0]])
                    te = np.array([te[1], te[2], te[0]])
                    tt = np.array([tt[1], tt[2], tt[0]])
                if tv[0] == tv[1]:
                    continue
                #print 'tv', tv-old_v_count
                #print 'tt', tt
                j0 = old_v_count-2-tv[0]
                j1 = old_v_count-2-tv[1]
                #print j0, j1
                while tt[0] != j0:
                    tv = np.array([tv[1], tv[2], tv[0]])
                    te = np.array([te[1], te[2], te[0]])
                    tt = np.array([tt[1], tt[2], tt[0]])
                if tt[1] != j1:
                    tv = np.array([tv[2], tv[0], tv[1]])
                    te = np.array([te[2], te[0], te[1]])
                    tt = np.array([tt[2], tt[0], tt[1]])
                j = tmp_v_count+new_vertex_count
                self.vertex[:,j] = self.vertex[:,(tt[0],tt[1])].mean(axis = 1)
                self.triangle[3*index[i]:3*index[i]+3] = tt[0], j, tt[2]
                self.triangle[3*next_triangle_index:3*next_triangle_index+3] = j, tt[1], tt[2]
                self.polygon_normal[:,next_triangle_index] = self.polygon_normal[:,index[i]]
                self.polygon_collocation[:,index[i]] = self.vertex[:,(tt[0],tt[2],j)].mean(axis = 1)
                self.polygon_collocation[:,next_triangle_index] = self.vertex[:,(tt[1],tt[2],j)].mean(axis = 1)
                self.polygon_area[index[i]] *= 0.5
                self.polygon_area[next_triangle_index] = self.polygon_area[index[i]]
                new_edge.extend([(tt[0], j), (tt[1], j), (tt[2], j)])
                new_edge_count += 3
                j = old_e_count+new_edge_count
                self.polygon_edge[3*index[i]:3*index[i]+3] = j-3, j-1, te[2]
                self.polygon_edge[3*next_triangle_index:3*next_triangle_index+3] = j-2, te[1], j-1
                new_vertex_count += 1
                next_triangle_index += 1
                continue
            if test == 1:
                #new_triangle_count -= 2
                while tv[2] >= old_v_count:
                    tv = np.array([tv[1], tv[2], tv[0]])
                    te = np.array([te[1], te[2], te[0]])
                    tt = np.array([tt[1], tt[2], tt[0]])
                #print tt, tv
                c0 = new_constant_p[tv[0]-old_v_count]
                c1 = new_constant_p[tv[1]-old_v_count]
                if self.edge[te[0]][0] == tt[0]:
                    c0 = 1-c0
                if self.edge[te[1]][0] == tt[1]:
                    c1 = 1-c1
                if abs(c1-(1-c0)*(1-c1)) < abs(1-c0-c0*c1):
                    self.triangle[3*index[i]:3*index[i]+3] = tt[0], tv[1], tt[2]
                    self.triangle[3*next_triangle_index:3*next_triangle_index+6] = tt[0], tv[0], tv[1], tv[0], tt[1], tv[1]
                    self.polygon_normal[:,next_triangle_index] = self.polygon_normal[:,index[i]]
                    self.polygon_normal[:,next_triangle_index+1] = self.polygon_normal[:,index[i]]
                    self.polygon_collocation[:,index[i]] = self.vertex[:,(tt[0],tv[1],tt[2])].mean(axis = 1)
                    self.polygon_collocation[:,next_triangle_index] = self.vertex[:,(tt[0],tv[0],tv[1])].mean(axis = 1)
                    self.polygon_collocation[:,next_triangle_index+1] = self.vertex[:,(tv[0],tt[1],tv[1])].mean(axis = 1)
                    old_area = self.polygon_area[index[i]]
                    self.polygon_area[[index[i], next_triangle_index, next_triangle_index+1]] = old_area*c1, old_area*(1-c0)*(1-c1), old_area*(1-c1)*c0
                    new_edge.extend([(tt[0], tv[1]), (min(tv[0], tv[1]), max(tv[0], tv[1]))])
                    new_edge_count += 2
                    if tt[1] > tt[2]:
                        tmp1 = 0
                    else:
                        tmp1 = 1
                    if tt[0] < tt[1]:
                        tmp0 = 0
                    else:
                        tmp0 = 1
                    self.polygon_edge[3*index[i]:3*index[i]+3] = old_e_count+new_edge_count-2, old_e_count+2*(tv[1]-old_v_count)+tmp1, te[2]
                    self.polygon_edge[3*next_triangle_index:3*next_triangle_index+3] = old_e_count+2*(tv[0]-old_v_count)+tmp0, old_e_count+new_edge_count-1, old_e_count+new_edge_count-2
                    self.polygon_edge[3*next_triangle_index+3:3*next_triangle_index+6] = old_e_count+2*(tv[0]-old_v_count)+1-tmp0, old_e_count+2*(tv[1]-old_v_count)+1-tmp1, old_e_count+new_edge_count-1
                else:
                    self.triangle[3*index[i]:3*index[i]+3] = tt[0], tv[0], tt[2]
                    self.triangle[3*next_triangle_index:3*next_triangle_index+6] = tt[2], tv[0], tv[1], tv[0], tt[1], tv[1]
                    self.polygon_normal[:,next_triangle_index] = self.polygon_normal[:,index[i]]
                    self.polygon_normal[:,next_triangle_index+1] = self.polygon_normal[:,index[i]]
                    self.polygon_collocation[:,index[i]] = self.vertex[:,(tt[0],tv[0],tt[2])].mean(axis = 1)
                    self.polygon_collocation[:,next_triangle_index] = self.vertex[:,(tt[2],tv[0],tv[1])].mean(axis = 1)
                    self.polygon_collocation[:,next_triangle_index+1] = self.vertex[:,(tv[0],tt[1],tv[1])].mean(axis = 1)
                    old_area = self.polygon_area[index[i]]
                    self.polygon_area[[index[i], next_triangle_index, next_triangle_index+1]] = old_area*(1-c0), old_area*c0*c1, old_area*(1-c1)*c0
                    new_edge.extend([(tt[2], tv[0]), (min(tv[0], tv[1]), max(tv[0], tv[1]))])
                    new_edge_count += 2
                    if tt[1] > tt[2]:
                        tmp1 = 0
                    else:
                        tmp1 = 1
                    if tt[0] < tt[1]:
                        tmp0 = 0
                    else:
                        tmp0 = 1
                    self.polygon_edge[3*index[i]:3*index[i]+3] = old_e_count+2*(tv[0]-old_v_count)+tmp0, old_e_count+new_edge_count-2, te[2]
                    self.polygon_edge[3*next_triangle_index:3*next_triangle_index+3] = old_e_count+new_edge_count-2, old_e_count+new_edge_count-1, old_e_count+2*(tv[1]-old_v_count)+tmp1
                    self.polygon_edge[3*next_triangle_index+3:3*next_triangle_index+6] = old_e_count+2*(tv[0]-old_v_count)+1-tmp0, old_e_count+2*(tv[1]-old_v_count)+1-tmp1, old_e_count+new_edge_count-1
                next_triangle_index += 2
                continue
            while tv[0] < old_v_count:
                tv = np.array([tv[1], tv[2], tv[0]])
                te = np.array([te[1], te[2], te[0]])
                tt = np.array([tt[1], tt[2], tt[0]])
            self.triangle[3*index[i]:3*index[i]+3] = tt[0], tv[0], tt[2]
            self.triangle[3*next_triangle_index:3*next_triangle_index+3] = tv[0], tt[1], tt[2]
            self.polygon_normal[:,next_triangle_index] = self.polygon_normal[:,index[i]]
            self.polygon_collocation[:,index[i]] = self.vertex[:,(tt[0],tv[0],tt[2])].mean(axis = 1)
            self.polygon_collocation[:,next_triangle_index] = self.vertex[:,(tv[0],tt[1],tt[2])].mean(axis = 1)
            c0 = new_constant_p[tv[0]-old_v_count]
            if self.edge[te[0]][0] == tt[0]:
                c0 = 1-c0
            old_area = self.polygon_area[index[i]]
            self.polygon_area[[index[i], next_triangle_index]] = old_area*(1-c0), old_area*c0
            new_edge.append((tt[2], tv[0]))
            #print i, (tt[2], tv[0])
            new_edge_count += 1
            if tt[0] < tt[1]:
                tmp0 = 0
            else:
                tmp0 = 1
            self.polygon_edge[3*index[i]:3*index[i]+3] = old_e_count+2*(tv[0]-old_v_count)+tmp0, old_e_count+new_edge_count-1, te[2]
            self.polygon_edge[3*next_triangle_index:3*next_triangle_index+3] = old_e_count+2*(tv[0]-old_v_count)+1-tmp0, te[1], old_e_count+new_edge_count-1
            next_triangle_index += 1
        #print 'divide 2', new_triangle_count
        #print 'last triangle index:', next_triangle_index
        #self.polygon_edge = np.concatenate([self.polygon_edge, new_polygon_edge])
        #print new_edge
        self.edge = np.concatenate([self.edge, new_edge])
        self.edge_count = self.edge.shape[0]
        t1 = time()
        #print 'divide 3', t1-t0
        self.polygon_count = self.triangle_count
        self.count = self.triangle_count
        self.vertex_count = self.vertex.shape[1]
        self.polygon = np.vstack([3*np.ones(self.triangle_count, dtype = np.int32), self.triangle.reshape(-1,3).T]).flatten('F')
        self.polygon_size = 3*np.ones(self.count, dtype = np.int32)
        self.polygon_start = np.arange(1, 4*self.count, 4, dtype = np.int32)
        #print new_constant_p
        #print edge_new_vertex_index
        #print new_vertex
        #print new_constant_p
        index = np.concatenate([index, np.arange(old_t_count, self.triangle_count, dtype = np.int32)])
        if index_start is None:
            s = u.dot(self.polygon_collocation)
        else:
            s = u.dot(self.polygon_collocation[:,index])
        permute = s.argsort()
        k = s[permute].searchsorted(u.dot(c))
        if index_start is None:
            return permute[:k], permute[k:]
        else:
            return index[permute[:k]], index[permute[k:]]
        
    def divide(self, index = None, tol = 1e-3):
        c, u = self.find_hyperplane(index)
        i0, i1 = self.divide_hyperplane(index, c, u, tol)
        return i0, i1, (c, u)

    def check_far(self, bb0, bb1):
        mean0 = bb0.mean(axis = 1)
        mean1 = bb1.mean(axis = 1)
        dist = np.linalg.norm(mean1-mean0)
        diag = max(np.linalg.norm(bb0[:,1]-bb0[:,0]), np.linalg.norm(bb1[:,1]-bb1[:,0]))
        return dist > 1*diag
    
    def bounding_box(self, index):
        ver_index = self.polygon[(np.array([1,2,3]).reshape(3,1)+4*index).flatten('F')]
        selected = self.vertex[:,ver_index]
        return np.hstack([selected.min(axis = 1).reshape(3,1), selected.max(axis = 1).reshape(3,1)])

    def is_dividable(self, index, max_area = None):
        if not (max_area is None):
            if self.polygon_area[index].max() > max_area:
                return True
        return False
