import numpy as np
cimport numpy as np
from struct import unpack
from libc.math cimport sqrt, log, sin, cos

cdef int inv_distance_c(void *obj1, int row_size, int *row, void *obj2, int col_size, int *col, double *out):
    cdef object pyobj1 = <object>obj1, pyobj2 = <object>obj2
    cdef int dim = <int>pyobj1.vertex.shape[0]
    cdef int dst_vertex_count = <int>pyobj1.vertex.shape[1]
    cdef double *dst_vertex = <double *><long>(pyobj1.vertex).ctypes.data
    cdef int src_vertex_count = <int>pyobj2.vertex.shape[1]
    cdef double *src_vertex = <double *><long>(pyobj2.vertex).ctypes.data
    cdef int i, j, k
    cdef double tmp, tmp0
    for i in range(col_size):
        for j in range(row_size):
            tmp = 0.0
            for k in range(dim):
                tmp0 = dst_vertex[row[j]+k*dst_vertex_count]-src_vertex[col[i]+k*src_vertex_count]
                tmp += tmp0*tmp0
            if tmp > 0:
                out[j*col_size+i] = 1.0/sqrt(tmp)
            else:
                out[j*col_size+i] = 0.0
    return 0

cdef int log_distance_c(void *obj1, int row_size, int *row, void *obj2, int col_size, int *col, double *out):
    cdef object pyobj1 = <object>obj1, pyobj2 = <object>obj2
    cdef int dim = <int>pyobj1.vertex.shape[0]
    cdef int dst_vertex_count = <int>pyobj1.vertex.shape[1]
    cdef double *dst_vertex = <double *><long>(pyobj1.vertex).ctypes.data
    cdef int src_vertex_count = <int>pyobj2.vertex.shape[1]
    cdef double *src_vertex = <double *><long>(pyobj2.vertex).ctypes.data
    cdef int i, j, k
    cdef double tmp, tmp0
    for i in range(col_size):
        for j in range(row_size):
            tmp = 0.0
            for k in range(dim):
                tmp0 = dst_vertex[row[j]+k*dst_vertex_count]-src_vertex[col[i]+k*src_vertex_count]
                tmp += tmp0*tmp0
            if tmp > 0:
                out[j*col_size+i] = 0.5*log(tmp)
            else:
                out[j*col_size+i] = 0.0
    return 0

cdef int exp_ikr_c(void *obj1, int row_size, int *row, void *obj2, int col_size, int *col, double complex *out):
    cdef object pyobj1 = <object>obj1, pyobj2 = <object>obj2
    cdef int dim = <int>pyobj1.vertex.shape[0]
    cdef int dst_vertex_count = <int>pyobj1.vertex.shape[1]
    cdef double *dst_vertex = <double *><long>(pyobj1.vertex).ctypes.data
    cdef int src_vertex_count = <int>pyobj2.vertex.shape[1]
    cdef double *src_vertex = <double *><long>(pyobj2.vertex).ctypes.data
    cdef int i, j, k
    cdef double tmp, tmp0, param_k = <double>pyobj1.param_k
    for i in range(col_size):
        for j in range(row_size):
            tmp = 0.0
            for k in range(dim):
                tmp0 = dst_vertex[row[j]+k*dst_vertex_count]-src_vertex[col[i]+k*src_vertex_count]
                tmp += tmp0*tmp0
            if tmp > 0:
                tmp0 = sqrt(tmp)
                tmp = param_k*tmp0
                out[j*col_size+i] = (cos(tmp)+1.0j*sin(tmp))/tmp0
            else:
                out[j*col_size+i] = 0.0+0.0j
    return 0

cdef int inv_dist_int_c(void *obj1, int row_size, int *row, void *obj2, int col_size, int *col, double *out):
    cdef object pyobj1 = <object>obj1, pyobj2 = <object>obj2
    cdef int dim = <int>pyobj1.vertex.shape[0]
    cdef int dst_vertex_count = <int>pyobj1.vertex.shape[1]
    cdef double *dst_vertex = <double *><long>(pyobj1.vertex).ctypes.data
    cdef int src_vertex_count = <int>pyobj2.vertex.shape[1]
    cdef double *src_vertex = <double *><long>(pyobj2.vertex).ctypes.data
    cdef int i, j, k
    cdef double tmp, tmp0
    for i in range(col_size):
        for j in range(row_size):
            tmp = 0.0
            for k in range(dim):
                tmp0 = dst_vertex[row[j]+k*dst_vertex_count]-src_vertex[col[i]+k*src_vertex_count]
                tmp += tmp0*tmp0
            if tmp > 0:
                out[j*col_size+i] = 0.5*log(tmp)
            else:
                out[j*col_size+i] = 0.0
    return 0

def inv_distance(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    inv_distance_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans

def log_distance(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    log_distance_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans

def inv_dist_int(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    inv_dist_int_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans

def exp_ikr(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.complex128)
    exp_ikr_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double complex*>(ans.data))
    return ans
    
class Data(object):
    def __init__(self, vertex, copy=False):
        if copy:
            self.vertex = vertex.copy()
        else:
            self.vertex = vertex
        self.count = self.vertex.shape[1]
        self.dim = self.vertex.shape[0]

    def __del__(self):
        pass

    @staticmethod
    def from_bin_file(f):
        fname = None
        if type(f) == str:
            fname = f
            try:
                f = open(fname, "rb")
            except:
                pass
        elif type(f) != file:
            print 'Not a file!'
            return
        shape0, shape1 = unpack("iii", f.read(12))
        vertex = np.fromfile(f, count = shape0*shape1, dtype = np.float64).reshape(shape0, shape1)
        if fname != None:
            f.close()
        return Data(vertex)
    
    @staticmethod
    def from_txt_file(f):
        fname = None
        if type(f) == str:
            fname = f
            try:
                f = open(fname, "r")
            except:
                pass
        elif type(f) != file:
            print 'Not a file!'
            return
        shape0, shape1 = map(int, f.readline().split())
        vertex = np.ndarray((shape0, shape1), dtype = np.float64)
        for i in xrange(shape1):
            vertex[:,i] = map(float, f.readline().split())
        if fname != None:
            f.close()
        return Data(vertex)

    def check_far(self, bb0, bb1):
        mean0 = bb0.mean(axis = 1)
        mean1 = bb1.mean(axis = 1)
        dist = np.linalg.norm(mean1-mean0)
        diag = max(np.linalg.norm(bb0[:,1]-bb0[:,0]), np.linalg.norm(bb1[:,1]-bb1[:,0]))
        return dist > 1.1*diag
    
    def bounding_box(self, index):
        selected = self.vertex[:,index]
        return np.hstack([selected.min(axis = 1).reshape(self.dim,1), selected.max(axis = 1).reshape(self.dim,1)])

    def is_dividable(self, index):
        return False
        
    def divide(self, index, tol):
        vertex = self.vertex[:, index].copy()
        center = vertex.mean(axis = 1)
        vertex -= center.reshape(self.dim, 1)
        normal = np.linalg.svd(vertex, full_matrices = 0)[0][:,0]
        scal_dot = normal.dot(vertex)
        permute = scal_dot.argsort()
        scal_dot = scal_dot[permute]
        k = scal_dot.searchsorted(0)
        return index[permute[:k]], index[permute[k:]], (center, normal)
