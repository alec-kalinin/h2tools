import numpy as np
cimport numpy as np
from libc.math cimport sqrt, fabs, M_PI
cimport cython
from libc.stdlib cimport malloc, free

cdef int electro_c(void *obj0, int row_size, int *row, void *obj1, int col_size, int *col, double *out):
    cdef object pyobj0 = <object>obj0
    cdef int count = <int>pyobj0.count
    cdef double *surface_x = <double *><long>(pyobj0.surface.ctypes.data)
    cdef double *surface_y = surface_x+count
    cdef double *surface_z = surface_y+count
    cdef double *normal_x = <double *><long>(pyobj0.normal.ctypes.data)
    cdef double *normal_y = normal_x+count
    cdef double *normal_z = normal_y+count
    cdef double *area = <double *><long>(pyobj0.area.ctypes.data)
    cdef double eps = <double>pyobj0.eps
    cdef int i, j
    cdef double *vec = [0,0,0], norm
    for i in range(col_size):
        for j in range(row_size):
            if row[j] == col[i]:
                out[j*col_size+i] = 0.0
                continue
            vec[0] = surface_x[row[j]]-surface_x[col[i]]
            vec[1] = surface_y[row[j]]-surface_y[col[i]]
            vec[2] = surface_z[row[j]]-surface_z[col[i]]
            norm = vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]
            norm = 1.0/(norm*sqrt(norm))
            vec[0] *= norm
            vec[1] *= norm
            vec[2] *= norm
            norm = vec[0]*normal_x[row[j]]+vec[1]*normal_y[row[j]]+vec[2]*normal_z[row[j]]
            out[j*col_size+i] = norm*area[row[j]]*(eps-1)/(4*M_PI*(eps+1))
    return 0

cdef int rhs_c(void *obj0, int row_size, int *row, void *obj1, int col_size, int *col, double *out):
    cdef object pyobj0 = <object>obj0
    cdef int count = <int>(pyobj0.count)
    cdef int acount = <int>(pyobj0.acount)
    cdef double *surface_x = <double *><long>(pyobj0.surface.ctypes.data)
    cdef double *surface_y = surface_x+count
    cdef double *surface_z = surface_y+count
    cdef double *atom_x = <double *><long>(pyobj0.atom.ctypes.data)
    cdef double *atom_y = atom_x+acount
    cdef double *atom_z = atom_y+acount
    cdef double *normal_x = <double *><long>(pyobj0.normal.ctypes.data)
    cdef double *normal_y = normal_x+count
    cdef double *normal_z = normal_y+count
    cdef double *area = <double *><long>(pyobj0.area.ctypes.data)
    cdef double eps = <double>(pyobj0.eps)
    cdef int i, j
    cdef double *vec = [0,0,0], norm
    for i in range(col_size):
        for j in range(row_size):
            vec[0] = surface_x[row[j]]-atom_x[col[i]]
            vec[1] = surface_y[row[j]]-atom_y[col[i]]
            vec[2] = surface_z[row[j]]-atom_z[col[i]]
            norm = vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]
            norm = 1.0/(norm*sqrt(norm))
            vec[0] *= norm
            vec[1] *= norm
            vec[2] *= norm
            norm = vec[0]*normal_x[row[j]]+vec[1]*normal_y[row[j]]+vec[2]*normal_z[row[j]]
            out[j*col_size+i] = norm*area[row[j]]*(1-eps)/(4*M_PI*(eps+1))
    return 0

cdef int energy_c(void *obj0, int row_size, int *row, void *obj1, int col_size, int *col, double *out):
    cdef object pyobj0 = <object>obj0
    cdef int count = <int>(pyobj0.count)
    cdef int acount = <int>(pyobj0.acount)
    cdef double *surface_x = <double *><long>(pyobj0.surface.ctypes.data)
    cdef double *surface_y = surface_x+count
    cdef double *surface_z = surface_y+count
    cdef double *atom_x = <double *><long>(pyobj0.atom.ctypes.data)
    cdef double *atom_y = atom_x+acount
    cdef double *atom_z = atom_y+acount
    cdef double *normal_x = <double *><long>(pyobj0.normal.ctypes.data)
    cdef double *normal_y = normal_x+count
    cdef double *normal_z = normal_y+count
    cdef double *area = <double *><long>(pyobj0.area.ctypes.data)
    cdef double eps = <double>(pyobj0.eps)
    cdef int i, j
    cdef double *vec = [0,0,0], norm
    for i in range(col_size):
        for j in range(row_size):
            vec[0] = surface_x[row[j]]-atom_x[col[i]]
            vec[1] = surface_y[row[j]]-atom_y[col[i]]
            vec[2] = surface_z[row[j]]-atom_z[col[i]]
            norm = vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]
            out[j*col_size+i] = 1.0/sqrt(norm)
    return 0

def electro(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    electro_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans
    
def rhs(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    rhs_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans
    
def energy(object obj1, int[:] row, object obj2, int[:] col):
    cdef int row_size = row.size, col_size = col.size
    cdef np.ndarray ans = np.ndarray((row_size, col_size), dtype = np.float64)
    energy_c(<void *>obj1, row_size, &row[0], <void *>obj2, col_size, &col[0], <double *>(ans.data))
    return ans

class Data(object):
    def __init__(self, count, surface, normal, area, acount, atom, acharge, eps):
        self.count = count
        self.surface = surface
        self.normal = normal
        self.area = area
        self.acount = acount
        self.atom = atom
        self.acharge = acharge
        self.eps = eps
        self.dim = 3
        
    @staticmethod
    def from_files(fname, eps):
        f = open(fname, 'r')
        acount = (int)(f.readline()[:-1])
        #print 'acount', acount
        atom = np.ndarray((3,acount), dtype = np.float64)
        acharge = np.ndarray(acount, dtype = np.float64)
        for i in xrange(acount):
            line = f.readline()
            t = line.split()
            atom[0,i], atom[1,i], atom[2,i] = (float)(t[0]), (float)(t[1]), (float)(t[2])
            acharge[i] = (float)(t[3])
        count = (int)(f.readline()[:-1])
        #print 'count', count
        surface = np.ndarray((3,count), dtype = np.float64)
        normal = np.ndarray((3,count), dtype = np.float64)
        area = np.ndarray(count, dtype = np.float64)
        for i in xrange(count):
            line = f.readline()
            t = line.split()
            surface[0,i], surface[1,i], surface[2,i] = (float)(t[0]), (float)(t[1]), (float)(t[2])
            normal[0,i], normal[1,i], normal[2,i] = (float)(t[3]), (float)(t[4]), (float)(t[5])
            area[i] = (float)(t[6])
        f.close()
        return Data(count, surface, normal, area, acount, atom, acharge, eps)

    def check_far(self, bb0, bb1):
        mean0 = bb0.mean(axis = 1)
        mean1 = bb1.mean(axis = 1)
        dist = np.linalg.norm(mean1-mean0)
        diag = max(np.linalg.norm(bb0[:,1]-bb0[:,0]), np.linalg.norm(bb1[:,1]-bb1[:,0]))
        return dist > diag
    
    def bounding_box(self, index):
        selected = self.surface[:,index]
        return np.hstack([selected.min(axis = 1).reshape(3,1), selected.max(axis = 1).reshape(3,1)])

    def is_dividable(self, index):
        return False

    def divide(self, index, tol):
        vertex = self.surface[:, index].copy()
        center = vertex.mean(axis = 1)
        vertex -= center.reshape(self.dim, 1)
        normal = np.linalg.svd(vertex, full_matrices = 0)[0][:,0]
        scal_dot = normal.dot(vertex)
        permute = scal_dot.argsort()
        scal_dot = scal_dot[permute]
        k = scal_dot.searchsorted(0)
        return index[permute[:k]], index[permute[k:]], (center, normal)
