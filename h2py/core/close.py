import numpy as np

class Factor(object):
    def __new__(cls, *args, **kwargs):
        return super(Factor, cls).__new__(cls)
    
    def __init__(self, dtype, func, row_data, row_tree, col_data, col_tree, onfly = False):
        self.dtype = dtype
        self.func = func
        self.row_data = row_data
        self.col_data = col_data
        self.onfly = onfly
        row = self.row_tree = row_tree
        col = self.col_tree = col_tree
        if onfly:
            return
        row_size = row.level[-1]
        col_size = col.level[-1]
        self.row_matrix = [[] for i in xrange(row_size)]
        self.col_matrix = [[] for i in xrange(col_size)]
        for i in xrange(row_size):
            for j in row.close[i]:
                self.row_matrix[i].append(func(row.index[i], col.index[j]))
        for i in xrange(col_size):
            for j in col.close[i]:
                k = row.close[j].index(i)
                self.col_matrix[i].append(self.row_matrix[j][k].T)

    def dot(self, x):
        row = self.row_tree
        col = self.col_tree
        row_data =  self.row_data
        col_data = self.col_data
        func = self.func
        row_size = row.level[-1]
        if x.ndim == 1:
            x = x.reshape(x.shape[0], 1)
        nrhs = x.shape[1]
        answer = np.zeros((row.index[0].shape[0], nrhs), dtype = self.dtype)
        if self.onfly:
            for i in xrange(row_size):
                for j in xrange(len(row.close[i])):
                    answer[row.index[i]] += func(row.index[i], col.index[row.close[i][j]]).dot(x[col.index[row.close[i][j]]])
        else:
            matrix = self.row_matrix
            for i in xrange(row_size):
                for j in xrange(len(row.close[i])):
                    answer[row.index[i]] += matrix[i][j].dot(x[col.index[row.close[i][j]]])
        if nrhs == 1:
            answer = answer.reshape(answer.shape[0])
        return answer

    def rdot(self, x):
        row = self.row_tree
        col = self.col_tree
        row_data =  self.row_data
        col_data = self.col_data
        func = self.func
        col_size = col.level[-1]
        if x.ndim == 1:
            x = x.reshape(x.shape[0], 1)
        nrhs = x.shape[1]
        answer = np.zeros((col.index[0].shape[0], nrhs), dtype = self.dtype)
        if self.onfly:
            for i in xrange(col_size):
                for j in xrange(len(col.close[i])):
                    answer[col.index[i]] += func(row.index[col.close[i][j]], col.index[i]).T.dot(x[row.index[col.close[i][j]]])
        else:
            matrix = self.col_matrix
            for i in xrange(col_size):
                for j in xrange(len(col.close[i])):
                    answer[col.index[i]] += matrix[i][j].dot(x[row.index[col.close[i][j]]])
        if nrhs == 1:
            answer = answer.reshape(answer.shape[0])
        return answer

    def nbytes(self):
        if self.onfly:
            return 0
        row = self.row_tree
        row_size = row.level[-1]
        mem = 0
        for i in self.row_matrix:
            for j in i:
                mem += j.size
        return mem

    @property
    def T(self):
        return self.transpose()
        
    def transpose(self):
        answer = Factor.__new__(Factor)
        answer.dtype = self.dtype
        answer.func = self.func
        col = answer.row_tree = self.col_tree
        row = answer.col_tree = self.row_tree
        answer.row_data = self.col_data
        answer.col_data = self.row_data
        answer.onfly = self.onfly
        size = row.level[-1]
        answer.col_matrix = [[] for i in xrange(size)]
        for i in xrange(size):
            for j in xrange(len(row.close[i])):
                answer.col_matrix[i].append(self.row_matrix[i][j])
        col = self.col_tree
        size = col.level[-1]
        answer.row_matrix = [[] for i in xrange(size)]
        for i in xrange(size):
            for j in xrange(len(col.close[i])):
                answer.row_matrix[i].append(self.col_matrix[i][j])
        return answer
