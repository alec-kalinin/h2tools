import numpy as np
from time import time
class Tree(object):
    def __init__(self, data, _data, **kwargs):
        self.data = data
        self._data = [_data]
        self.index = [np.arange(data.count, dtype = np.int32)]
        #print kwargs
        self.block_size = kwargs.pop('block_size', 100)
        self.divide_tol = kwargs.pop('tol', 1e-3)
        self.check_params = kwargs
        self.far = [[]]
        self.close = [[]]
        self.parent = [-1]
        self.child = [[]]
        self.level = [0, 1]
        self.division = []
        self.notransition = [True]
        
    def __del__(self):
        pass
    def __copy__(self):
        pass
    
    def update_index(self):
        for i in xrange(self.level[-1]-1, -1, -1):
            tmp = []
            for j in self.child[i]:
                tmp.extend(self.index[j])
            if len(tmp) > 0:
                self.index[i] = np.array(tmp, dtype = np.int32)

    def add_node(self, index, parent):
        i = len(self.index)
        self.index.append(np.array(index, dtype = np.int32))
        self._data.append(self.data.bounding_box(index))
        self.parent.append(parent)
        self.child[parent].append(i)
        self.child.append([])
        self.close.append([])
        self.far.append([])

    def build(self, other):
        if other is self:
            self.build1()
        
    def build1(self):
        _close = [[0]]
        _divide = [False]
        fix_dividable = not hasattr(self.data, 'is_dividable')
        #print fix_dividable
        if self.index[0].size > self.block_size:
            _dividable = [True]
        elif fix_dividable:
            _dividable = [False]
        else:
            _dividable = [self.data.is_dividable(self.index[0], **self.check_params)]
        t1 = 0
        t2 = 0
        t3 = 0
        t4 = 0
        t20 = 0
        #print 'start', self.level[-2], self.level[-1]
        while self.level[-2] < self.level[-1]:
            #print 'NEW LVL'
            t0 = time()
            for i in xrange(self.level[-2], self.level[-1]):
                for j in _close[i]:
                    if self.data.check_far(self._data[i], self._data[j]):
                        self.far[i].append(j)
                    elif _dividable[i] and _dividable[j]:
                        _divide[i] = True
                    else:
                        self.close[i].append(j)
                _close[i] = list(set(_close[i])-set(self.far[i]+self.close[i]))
                #print '__close', _close[i]
            t1 += time()-t0
            t0 = time()
            for i in xrange(self.level[-2], self.level[-1]):
                if i > 0:
                    self.notransition.append(not(len(self.far[i]) > 0 or not self.notransition[self.parent[i]]))
                if _divide[i]:
                    t00 = time()
                    _index = list(self.data.divide(self.index[i], tol=self.divide_tol))
                    t20 += time()-t00
                    self.division.append(_index.pop())
                    for k in _index:
                        self.add_node(k, i)
                        if k.size > self.block_size:
                            _dividable.append(True)
                        elif fix_dividable:
                            _dividable.append(False)
                        else:
                            _dividable.append(self.data.is_dividable(k, **self.check_params))
                        _close.append([])
                else:
                    self.division.append(None)
            t2 += time()-t0
            t0 = time()
            for i in xrange(self.level[-2], self.level[-1]):
                tmp = []
                #print '_close:',_close[i]
                for j in _close[i]:
                    #print '-', self.child[j]
                    tmp.extend(self.child[j])
                #print tmp
                for j in self.child[i]:
                    #print j
                    _close[j].extend(tmp)
                    #print 'j',j, _close[j]
            t3 += time()-t0
            t0 = time()
            self.level.append(len(self.parent))
            _divide.extend([False for ii in xrange(self.level[-1]-self.level[-2])])
            t4 += time()-t0
        t0 = time()
        self.update_index()
        t4 += time()-t0
        #print t1,t2,'(',t20,')',t3,t4
        self._close = _close
            
    def __getitem__(self, i):
        return self.data[self.index[i]]
    
